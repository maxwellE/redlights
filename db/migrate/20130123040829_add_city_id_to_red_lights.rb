class AddCityIdToRedLights < ActiveRecord::Migration
  def change
    add_column :red_lights, :city_id, :integer
  end
end
