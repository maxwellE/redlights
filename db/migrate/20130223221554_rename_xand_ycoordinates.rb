class RenameXandYcoordinates < ActiveRecord::Migration
  def change
    rename_column :red_lights,:ycoordinate, :longitude
    rename_column :red_lights,:xcoordinate, :latitude
  end
end
