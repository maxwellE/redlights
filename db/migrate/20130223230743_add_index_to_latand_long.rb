class AddIndexToLatandLong < ActiveRecord::Migration
  def change
    add_index :red_lights, :latitude
    add_index :red_lights, :longitude
  end
end
