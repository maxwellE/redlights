class CreateRedLights < ActiveRecord::Migration
  def change
    create_table :red_lights do |t|
      t.string :name
      t.float :xcoordinate
      t.float :ycoordinate

      t.timestamps
    end
  end
end
