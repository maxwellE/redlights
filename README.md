RedLights API Reference
=======================

Routes
--------

#### GET /in_proximity_of

##### Parameters
* latitude, latitude of current posistion (floating point value)
* longitude, longitude of current posistion (floating point value)
* distance_in_miles, it is what is says (integer value)

Example:

http://ec2-54-215-161-95.us-west-1.compute.amazonaws.com:3000/in_proximity_of?latitude=40&longitude=-83&distance_in_miles=10