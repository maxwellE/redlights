require 'test_helper'

class RedLightTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "should collect a subset of points when called by proximity" do
    assert_equal(2, RedLight.in_proximity_of(1.5,1.5,1).count)
  end
end
