require 'test_helper'

class RedlightsControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get in_proximity_of" do
    get :in_proximity_of
    assert_response :success
  end

end
