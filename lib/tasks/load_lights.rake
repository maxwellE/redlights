# encoding: utf-8
namespace :load do
  desc "Load red lights from the USA"
  task :red_lights_usa => :environment do
    require 'mechanize'
    SKIP_LINK_LIST = [
      "Never get a Red Light Ticket again",
       "Redlight Camera List - GPS POIs",
       "The List",
       "FREE Samples",
       "How it Works. How the Redlight Camera List Works",
       "Redlight Camera Videos",
       "Documentation",
       "Support",
       "F.A.Q.",
       "About Us",
       "List",
        "Legal",
       "Privacy",
       "Contact",
       "Sitemap"
    ]
    SKIP_LINK_LIST_CITY = [
      "United States of America",
      "List",
      "Download Free Sample"
    ]
    agent = Mechanize.new
    page = agent.get("http://www.redlightcameralist.com/poi/United-States-of-America/")
    page.links.each do |state_link|
      next if SKIP_LINK_LIST.include? state_link.text
      state = State.where(:name => state_link.text).first_or_create
      puts state.name
      cities_page = state_link.click
      cities_page.links.each do |city_link|
        next if SKIP_LINK_LIST.include? city_link.text
        next if SKIP_LINK_LIST_CITY.include? city_link.text 
        city = state.cities.where(:name => city_link.text).first_or_create
        puts city.name
        red_lights_page = city_link.click
        # define recursive function here to paginate
        grab_lights(red_lights_page,city,0)
      end
    end
  end
  def grab_lights(light_page,city,last_index)
    parsed_lights = light_page.search("span.link").map do |light_elem| 
      raw_str = light_elem.attributes["onclick"].value
      parse_light(raw_str)
    end
    current_index = light_page.at("li.current") ? light_page.at("li.current").text.to_i : nil
    if current_index.nil?
      parsed_lights.each do |light|
        city.red_lights.where(:name => light[:name], :latitude => light[:xcoordinate], :longitude => light[:ycoordinate]).first_or_create
      end
      return true
    elsif current_index == last_index
      return true
    else
      parsed_lights.each do |light|
        city.red_lights.where(:name => light[:name], :latitude => light[:xcoordinate], :longitude => light[:ycoordinate]).first_or_create
      end
      next_page = light_page.links.find{|x| x.text == "» previous"}.click
      grab_lights(next_page,city,current_index)
    end
  end
  def parse_light(light_string)
    light_string =~ /\AsetLocation\(([^,]+), ([^,]+), '([^']+)/
    {
      :xcoordinate => $1,
      :ycoordinate => $2,
      :name => $3
    }
  end
end
