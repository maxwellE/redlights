class RedlightsController < ApplicationController
  def all
    @lights = RedLight.includes(:city => [:state])
    render :json => jsonify(@lights)
  end
  
  def in_proximity_of
    @lights = RedLight.in_proximity_of(params[:latitude].to_f,
                                        params[:longitude].to_f,params[:distance_in_miles].to_i).includes(:city => [:state])
    render :json => jsonify(@lights)
  end
  
  private 
  def jsonify(lights)
    Jbuilder.encode do |json|
      json.array! lights do |light|
        json.state light.city.state.name
        json.city light.city.name
        json.name light.name
        json.longitude light.longitude
        json.latitude light.latitude
        json.id light.id
      end
    end
  end
end
