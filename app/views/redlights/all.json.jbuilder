json.array! @lights do |light|
  json.state light.city.state.name
  json.city light.city.name
  json.name light.name
  json.longitude light.longitude
  json.latitude light.latitude
  json.id light.id
end