class RedLight < ActiveRecord::Base
  belongs_to :city
  attr_accessible :name, :latitude, :longitude,:city_id
  reverse_geocoded_by :latitude, :longitude
  def self.in_proximity_of(lat,long,distance_in_miles)
    RedLight.near([lat,long],distance_in_miles)
  end
end
