class City < ActiveRecord::Base
  belongs_to :state
  has_many :red_lights
  attr_accessible :name, :state_id
end
